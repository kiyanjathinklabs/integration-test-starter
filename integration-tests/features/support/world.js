// features/support/world.js
const {
  setWorldConstructor,
  World,
  setDefaultTimeout
} = require("@cucumber/cucumber")
const { Builder } = require('selenium-webdriver')
const firefox = require('selenium-webdriver/firefox')

const SCREEN = {
  width: 640,
  height: 480,
}

class CustomWorld extends World {
  driver = process.env.HEADLESS === "true" ? 
    new Builder().forBrowser('firefox')
      .setFirefoxOptions(new firefox.Options().headless().windowSize(SCREEN))
      .build() :
    new Builder().forBrowser('firefox').build()

  constructor(options) {
    super(options)
  }
}

setWorldConstructor(CustomWorld)

// Accommodates slugishness in gitlab runners
// Find a better solution for sizeable projects
setDefaultTimeout(60 * 1000)
