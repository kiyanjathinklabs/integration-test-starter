# Cucumber tests in GitLab CI with Gatsby

## Project overview

This project shows how to get some basic
Cucumber tests running automatically in GitLab's CI.
The tests are run against a simple GatsbyJS server.

For scalability and control over the environment,
we run the test in Docker containers.

The Gatsby site is just the simple `hello-world` starter
with an `id` added to the HTML element.

Complete the setup steps described below,
then execute:

```shell
docker-compose up --exit-code-from test-runner
```

## First time setup

### Your own repo

To see the CI work, you'll want to have your own copy of this project.

You can fork this repo or copy the files (excluding the `.git/` folder)
into an empty Gitlab repo of your own.

Make sure the `CI` is set to run.
In GitLab, select your project and enable shared runners.
The option is under:

```
Settings > CI/CD > Runners
```

### Optional steps

This project doesn't require you to do anything with Gatsby
or to run the integration tests outside of a docker context.
But doing so will allow you to:

- Modify the website.
- Serve the website locally.
- Run integration tests outside of the docker context.

Here are the setup steps.
Or just reverse engineer them from the `Dockerfile`s.

1. Install `node`. See [Gatsby's setup docs][gatsby_setup].
1. `npm install -g gatsby-cli`. See [Gatsby's setup docs][gatsby_setup].
1. Install `geckodriver` and put it in the `PATH`. See `integration-tests/Dockerfile`.
1. `npm install` in both `hello-world/` and `integration-tests/`.

### Docker

You'll need Docker and Docker Compose for this project.

1. [Install Docker][docker_install]
1. [Install Docker Compose][docker_compose_install]

## Files of note

```
# Docker setup
hello-world/Dockerfile
hello-world/.dockerignore
integration-tests/Dockerfile
integration-tests/.dockerignore

# Docker Compose setup
docker-compose.yaml

# Gitlab CI
.gitlab-ci.yaml
```

## Testing without Docker

You can run the tests without Docker.
Follow the setup instructions described above.
Open two terminal windows.
From the project dir:

```shell
# First terminal
cd hello-world
gatsby serve
```

```shell
# Second terminal
cd integration-tests
TEST_SERVER=http://localhost:9000 npm run test
```

## Troubleshooting

### Can't find Docker

If your system can't find Docker,

1. Ensure Docker is installed, following the [steps in the docs][docker_install].
1. Ensure your user is part of the docker group (see the docs).
1. Turn it off and on again after adding your user to the docker group.

### Docker Compose fails

Make sure you're using a version that's compatible with the example.

```shell
>  docker-compose version
docker-compose version 1.27.4, build 40524192
docker-py version: 4.3.1
CPython version: 3.7.7
OpenSSL version: OpenSSL 1.1.0l  10 Sep 2019
```

### Tests fail locally

Is there a timeout?
Try setting the command for the `test-runner` to:

```
["curl", "--fail", "http://test-server:9000"]
```

This can be done in either
`integration-tests/Dockerfile` using `CMD` or
`docker-compose.yaml` using `command:` under the `test-runner`

If necessary, you can hop into the running containers.
Start them with `docker-compose up`.
Login using the `docker` command with the `-it` flags.

### Tests fail in GitLab CI

Assuming everything works locally,
check your local docker and docker compose versions.
Ensure GitLab CI is using the same versions.

### Docker doesn't update after code change

Rebuild your images:

```shell
docker-compose build test-server test-runner
```

[docker_install]: https://docs.docker.com/get-docker/
[docker_compose_install]: https://docs.docker.com/compose/install/
[gatsby_setup]: https://www.gatsbyjs.com/docs/tutorial/part-zero/
